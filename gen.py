day = input("for what day do you want to generate a new file? ")

code = f"""\
test = True
first_exercise = True

def main():
    filedir = f"inputs/day{day}{{('a' if first_exercise and test else 'b') if test else ''}}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        # text = f.read()
        ...
    
        
    if first_exercise:
        ...
    else:
        ...

        
if __name__ == "__main__":
    exit(main())
"""

with open(f"day{day}.py",mode="w") as f:
    f.write(code)

with open(f"inputs/day{day}.txt",mode="w"):
    ...

with open(f"testinputs/day{day}a.txt", mode="w"):
    ...

with open(f"testinputs/day{day}b.txt", mode="w"):
    ...