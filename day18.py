test = False
first_exercise = False


def edgelength(e):
    a, b = e
    y1, x1 = a
    y2, x2 = b
    return abs(y1 - y2) + abs(x1 - x2)



def main():
    filedir = (
        f"inputs/day18{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...

    area = 0
    x = 0
    y = 0
    l = 0
    if first_exercise:
        dirs = {"R": (0, 1), "L": (0, -1), "U": (-1, 0), "D": (1, 0)}
        for line in lines:
            dc, n, _ = line.strip().split(" ")
            dx, dy = dirs[dc]
            nx, ny = x + int(n) * dx, y + int(n) * dy
            da = (x + nx) * (y - ny)
            print(da)
            area += da // 2
            l += int(n)
            y = ny
            x = nx
        area = abs(area)
        area += l // 2 + 1
        print(area)

    else:
        dirs = {"0": (0, 1), "1": (1, 0), "2": (0, -1), "3": (-1, 0)}
        for line in lines:
            _,word = line.strip().split("#")
            n = int(word[:-2],16)
            dx, dy = dirs[word[-2]]
            nx, ny = x + int(n) * dx, y + int(n) * dy
            da = (x + nx) * (y - ny)
            print(da)
            area += da // 2
            l += int(n)
            y = ny
            x = nx
        area = abs(area)
        area += l // 2 + 1
        print(area)


if __name__ == "__main__":
    exit(main())
