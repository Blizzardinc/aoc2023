test = False
first_exercise = False


def main():
    filedir = (
        f"inputs/day3{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...

    symbs = []
    directions = [(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1)]

    for i, line in enumerate(lines):
        lines[i] = line.strip()
        for j, c in enumerate(line.strip()):
            if c not in ".0123456789":
                symbs.append((i, j))

    digits = []
    for i, j in symbs:
        for a, b in directions:
            x = i + a
            y = j + b

            if (
                (0 <= x < len(lines))
                and (0 <= y < len(lines[x]))
                and lines[x][y] in "0123456789"
            ):
                digits.append((x, y))

    i = 0
    while i < len(digits):
        (x, y) = digits[i]
        if y-1 >= 0 and ((x, y - 1) not in digits) and lines[x][y - 1] in "0123456789":
            digits.append((x, y - 1))

        if y+1 < len(lines[x]) and ((x, y + 1) not in digits) and lines[x][y + 1] in "0123456789":
            digits.append((x, y + 1))
            print("+1")
        i+=1

    digits.sort()

    print(digits)
    nums = []
    x, first = digits[0]
    last = first
    for i, j in digits:
        if (i == x) and (j == last + 1):
            last += 1
        else:
            nums.append((x, first, last))
            x = i
            first = (last := j)
    nums.append((x,first,last))
    nums = nums[1:]

    if first_exercise:
        total = 0
        for i, j1, j2 in nums:
            print(int("".join(lines[i][j1:j2+1])))
            total += int("".join(lines[i][j1 : j2 + 1]))
    else:
        total = 0
        gears = [(x,y) for (x,y) in symbs if lines[x][y]=="*"]

        for (i,j) in gears:
            meshing = [(x,y1,y2) for (x,y1,y2) in nums if x-1 <= i <= x+1 and y1 - 1 <= j <= y2 + 1]
            if len(meshing) == 2:
                vals = [int("".join(lines[i][j1:j2+1])) for (i,j1,j2) in meshing]
                total += vals[0] * vals[1]
        
    print(total)

if __name__ == "__main__":
    exit(main())
