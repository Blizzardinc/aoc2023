test = False
first_exercise = False


def main():
    filedir = (
        f"inputs/day11{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    m = list(text.split("\n"))

    if first_exercise:
        expandedymap = []
        galaxies = []
        for line in m:
            expandedymap.append(line)
            if all((c == "." for c in line)):
                expandedymap.append(line)
        expandedxmap = [[] for _ in expandedymap]
        for i in range(len(expandedymap[0])):  # for every column
            extra = all(
                (line[i] == "." for line in expandedymap)
            )  # check if it is empty
            for y, line in enumerate(expandedymap):  # for every line
                if line[i] == "#":
                    galaxies.append((y, len(expandedxmap[y])))
                expandedxmap[y].append(line[i])  # add the char in that line
                if extra:
                    expandedxmap[y].append(".")
        galaxies.sort()
        # print(expandedxmap)
        print("\n".join(("".join(line) for line in expandedxmap)))
        # print(galaxies)
        # print(len([(x,y) for x in galaxies for y in galaxies if x != y]))
    else:
        expandingrows = []
        expandingcolumns = []
        galaxies = []
        for i, line in enumerate(m):
            for j, c in enumerate(line):
                if c == "#":
                    galaxies.append((i, j))
            if all([c == "." for c in line]):
                expandingrows.append(i)
        for j in range(len(m[0])):
            if all([line[j] == "." for line in m]):
                expandingcolumns.append(j)
        galaxies = [
            (
                i + (1_000_000-1) * len([x for x in expandingrows if x < i]),
                j + (1_000_000-1) * len([y for y in expandingcolumns if y < j]),
            )
            for (i, j) in galaxies
        ]
    print(
        sum((abs(a - x) + abs(b - y) for (a, b) in galaxies for (x, y) in galaxies))
        // 2
    )


if __name__ == "__main__":
    exit(main())
