test = False
first_exercise = False


def main():
    filedir = (
        f"inputs/day5{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    sections = text.split("\n\n")
    seeds = [int(word.strip()) for word in sections[0][7:].strip().split(" ")]
    maps = [
        [
            (*(int(num) for num in word.split(" ")),)
            for word in section.strip().split("\n")[1:]
        ]
        for section in sections[1:]
    ]

    if first_exercise:
        for map in maps:
            for i,v in enumerate(seeds):
                for (tstart,fstart,l) in map:
                    if fstart <= v < fstart + l:
                        seeds[i] = v - fstart +tstart
                        break
                
        print(min(seeds))
    else:
        seedranges = [(a,a+b) for (a,b) in zip(seeds[0::2],seeds[1::2])]
        print(seedranges)
        print(sum(seeds[1::2]))
        for i,map in enumerate(maps):
            ranges = []
            while seedranges != []:
                (x,y) = seedranges.pop()
                for (tstart,fstart,l) in map:
                    fend = fstart+l
                    if y <= fstart:
                        # the mapping range is right of the seed range
                        continue
                    elif fend <= x:
                        # the mapping range is to the left of the seed range
                        continue
                    else:
                        add_first_range= False
                        add_third_range=False
                        if x < fstart:
                            # the mapping range starts within the start range
                            seedranges.append((x,fstart))
                            add_first_range = True
                        
                        if fend < y:
                            # the mapping range ends within the seed range:
                            seedranges.append((fend,y))
                            add_third_range = True
                        # print((x,y),(fstart,fend))
                        (a,b) = (max(x,fstart),min(y,fend))
                        assert (a<b)
                        ranges.append((a-fstart+tstart,b-fstart+tstart))
                        
                        break
                else:
                    ranges.append((x,y))


            # print(ranges)
            seedranges = ranges
        print(sum([y-x for x,y in ranges]))
        print(ranges)
        print(min([x for x,y in ranges]))


if __name__ == "__main__":
    exit(main())
