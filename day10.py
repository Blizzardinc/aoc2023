test = False
first_exercise = False


def get_directions(char):
    res = []
    if char in "S|JL":
        res.append((-1, 0))
    if char in "S-J7":
        res.append((0, -1))
    if char in "S|7F":
        res.append((1, 0))
    if char in "S-FL":
        res.append((0, 1))
    return res

def main():
    filedir = (
        f"inputs/day10{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    map = text.split("\n")

    for i, line in enumerate(map):
        for j, char in enumerate(line):
            if char == "S":
                start = (i, j)

    visited = set()



    if first_exercise:
        positions = [start]
        previous = []
        i =-1
        while positions != []:
            i+=1
            visited.update(positions)
            previous = positions
            positions = [
                (i + a, j + b)
                for (i, j) in positions
                for (a, b) in get_directions(map[i][j])
                if (
                    (i + a, j + b) not in visited
                    and (-a, -b) in get_directions(map[i + a][j + b])
                )
            ]
        print(i)
    else:
        position = start
        area = 0
        # area = internal + vertices/2 -1
        # also area = 
        vertices=0
        while True:
            i,j = position
            nexts = [
                (i + a, j + b)
                for (a, b) in get_directions(map[i][j])
                if (
                    (i + a, j + b) not in visited
                    and (-a, -b) in get_directions(map[i + a][j + b])
                )
                ]

            if nexts != []:
                visited.add(position)
                (ny,x) = nexts[0]
                area += ((i+ny)*(j-x))//2
                vertices += 1
                position = (ny,x)
            else:
                area += ((ny+start[0])*(x-start[1]))//2
                vertices += 1
                break
        area = abs(area)
        print(f"{area=}")
        print(f"{vertices=}")
        print(f"{area-vertices//2+1=}")


if __name__ == "__main__":
    exit(main())
