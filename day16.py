from time import sleep


test = False
first_exercise = False

def step(m,pos,dir):
    (y,x) = pos
    (dy,dx) = dir

    ny = y+dy
    nx = x+dx
    newpos = (ny,nx)

    res = []
    match m[ny][nx]:
        case ".":
            return [(newpos,dir)]
        case "|":
            if dy == 0:
                return [(newpos,(1,0)),(newpos,(-1,0))]
            else:
                return [(newpos,dir)]
        case "-":
            if dx == 0:
                return [(newpos,(0,1)),(newpos,(0,-1))]
            else:
                return [(newpos,dir)]
        case "\\":
            return [(newpos,(dx,dy))]
        
        case "/":
            return [(newpos,(-dx,-dy))]

        case _:
            return []

def calc_energised(m,pos,dir):
    y_max = len(m)
    x_max = len(m[0])
    energised = set()
    visited = set()
    stack = [((pos[0]-dir[0],pos[1]-dir[1]),dir)]
    while stack != []:
        (pos,dir) = stack.pop()
        if (pos,dir) in visited:
            continue
        else:
            visited.add((pos,dir))
        energised.add(pos)
        if (not 0<= pos[0] + dir[0]< y_max) or (not 0<= pos[1] + dir[1] < x_max):
            continue
        stack += step(m,pos,dir)
        # print(stack)
        # sleep(2)
    return (len(energised)-1)

def main():
    filedir = f"inputs/day16{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    
    m = text.split("\n")



        
    if first_exercise:
        place = (0,0)
        direction = (0,1)
        print(calc_energised(m,place,direction))
    else:
        x_max = len(m[0])
        y_max = len(m)

        max_from_top = max([calc_energised(m,(0,x),(1,0)) for x in range(x_max)])
        max_from_bottom = max([calc_energised(m,(y_max-1,x),(-1,0)) for x in range(x_max)])
        max_from_left = max([calc_energised(m,(y,0),(0,1)) for y in range(y_max)])
        max_from_right = max([calc_energised(m,(y,x_max-1),(0,-1)) for y in range(y_max)])

        print((max_from_left,max_from_right,max_from_top,max_from_bottom))
        print([calc_energised(m,(0,x),(1,0)) for x in range(x_max)])
        print(max((max_from_left,max_from_right,max_from_top,max_from_bottom)))
        
if __name__ == "__main__":
    exit(main())
