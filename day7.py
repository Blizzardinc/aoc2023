test = False
first_exercise = False

from collections import Counter

def card_key(c):
    try:
        return int(c)
    except ValueError:
        match c:
            case "T":
                return 10
            case "J":
                return 11
            case "Q":
                return 12
            case "K":
                return 13
            case "A":
                return 14
            case _:
                ...

def hand_type(a):
    c = Counter(a)
    vs = sorted([*dict(c).values()],reverse=True)
    # print(vs)
    match vs:
        case [5]:
            return (6,[card_key(c) for c in a], "five_of_a_kind")
        case [4, 1]:
            return (5,[card_key(c) for c in a], "four_of_a_kind")
        case [3, 2]:
            return (4,[card_key(c) for c in a], "full_house")
        case [3, 1, 1]:
            return (3,[card_key(c) for c in a], "set")
        case [2, 2, 1]:
            return (2,[card_key(c) for c in a], "two_pair")
        case [2, 1, 1, 1]:
            return (1,[card_key(c) for c in a], "one_pair")
        case _:
            return (0,[card_key(c) for c in a], "high_card")

def card_key2(c):
    try:
        return int(c)+1
    except ValueError:
        match c:
            case "T":
                return 11
            case "J":
                return 1
            case "Q":
                return 12
            case "K":
                return 13
            case "A":
                return 14
            case _:
                ...

def hand_type2(a):
    c = Counter(a)

    vs = sorted([v for k,v in c.items() if k != "J"],reverse=True)
    j= c["J"]
    if vs:
        vs[0]+=j
    else:
        vs = [j]

    # print(vs)
    match vs:
        case [5]:
            return (6,[card_key2(c) for c in a], "five_of_a_kind")
        case [4, 1]:
            return (5,[card_key2(c) for c in a], "four_of_a_kind")
        case [3, 2]:
            return (4,[card_key2(c) for c in a], "full_house")
        case [3, 1, 1]:
            return (3,[card_key2(c) for c in a], "set")
        case [2, 2, 1]:
            return (2,[card_key2(c) for c in a], "two_pair")
        case [2, 1, 1, 1]:
            return (1,[card_key2(c) for c in a], "one_pair")
        case _:
            return (0,[card_key(c) for c in a], "high_card")



def main():
    filedir = (
        f"inputs/day7{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...

    lines = [((x:= line.split(" "))[0],int(x[1]))for line in lines]

    if first_exercise:
        lines.sort(key=lambda x: hand_type(x[0]))
        ...
    else:
        lines.sort(key=lambda x: hand_type2(x[0]))
        ...
    print(sum((b[1]*(r+1) for r,b in enumerate(lines))))


if __name__ == "__main__":
    exit(main())
