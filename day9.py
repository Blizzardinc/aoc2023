test = False
first_exercise = False

def main():
    filedir = f"inputs/day9{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...
    sequences = ([int(word) for word in line.split(" ")] for line in lines)
    
    diffs =[]
    for seq in sequences:
        diff = [seq]
        while len(diff[-1])!=1:
            diff.append([a-b for (a,b) in zip(diff[-1][1:],diff[-1][:-1])])
        diffs.append(diff)
        
    if first_exercise:
        print(sum([sum([co[-1] for co in diff]) for diff in diffs]))
    else:
        print([sum([(-1)**i*co[0] for i,co in enumerate(diff)]) for diff in diffs])
        print(sum([sum([(-1)**i*co[0] for i,co in enumerate(diff)]) for diff in diffs]))

        
if __name__ == "__main__":
    exit(main())
