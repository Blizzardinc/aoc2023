from typing import Iterable, Callable, Self, Any, List

from itertools import chain


type ParseResult[S, A] = List[tuple[A, List[S]]]
type ParserT[S, A] = Callable[[List[S]], ParseResult[S, A]]


class Parser[S, A]:
    def __init__(self: Self, p: ParserT[S, A]) -> None:
        self.parse = p

    def __call__(self: Self, string: List[S]) -> ParseResult[S, A]:
        return self.parse(string)

    def __or__(self: Self, other: "Parser[S, A]") -> "Parser[S, A]":
        """`self | other` equivalent to haskell <|>"""

        def parser(string: List[S]) -> ParseResult[S, A]:
            return self.parse(string) + other.parse(string)

        return Parser(parser)

    def __lshift__(self: Self, other: "Parser[S, A]") -> "Parser[S, A]":
        """`self << other` equivalent to haskell <<|>"""

        def parser(string: List[S]) -> ParseResult[S, A]:
            a = self.parse(string)
            if a:
                return a
            else:
                return other.parse(string)

        return Parser(parser)
    
    def __mul__(self,other):
        return other.__rmul__(self)

    def __rmul__[B](self: Self, other: "Parser[S, Callable[[A], B]]") -> "Parser[S, B]":
        """`other * self` equivalent to haskell <*>"""

        def parser(string: List[S]) -> ParseResult[S, B]:
            return [
                (f(b), tail2)
                for f, tail1 in other.parse(string)
                for b, tail2 in self.parse(tail1)
            ]

        return Parser(parser)
        # x = lambda string: ((f(b),tail1) for (f,tail1) in other.parse(string) for (b,tail2) in self.parse(tail1))
        # return Parser(x)

    def __ge__[B](self: Self, other: Callable[[A], "Parser[S,B]"]) -> "Parser[S,B]":
        """`self >= other`equivalent to haskell >>= (turbofish)"""

        def parser(string: List[S]) -> ParseResult[S, B]:
            return [
                (b, tail2)
                for a, tail1 in self.parse(string)
                for (b, tail2) in other(a).parse(tail1)
            ]

        return Parser(parser)

    def __rmod__[B](self: Self, other: Callable[[A], B]) -> "Parser[S,B]":
        """`other % self` equivalent to haskell `<$>`"""

        def parser(string: List[S]) -> ParseResult[S, B]:
            return [(other(a), tail1) for a, tail1 in self.parse(string)]

        return Parser(parser)

    # derived methods:
    def fmapLeft[B](self: Self, other: B) -> "Parser[S,B]":
        return (lambda x: other) % self

    def liftBinary[
        B, C
    ](self: Self, f: Callable[[A, B], C], other: "Parser[S,B]") -> "Parser[S,C]":
        def parser(string: List[S]) -> ParseResult[S, C]:
            return [
                (f(a, b), tail2)
                for a, tail1 in self.parse(string)
                for b, tail2 in other.parse(tail1)
            ]

        return Parser(parser)

    def thenIgnore[B](self: Self, other: "Parser[S,B]") -> "Parser[S,A]":
        def parser(string: List[S]) -> ParseResult[S,A]:
            return [(a,tail2) for a,tail1 in self.parse(string) for b,tail2 in other.parse(tail1)]
        return Parser(parser)

    def ignoreThen[B](self: Self, other: "Parser[S,B]") -> "Parser[S,B]":
        def parser(string: List[S]) -> ParseResult[S,B]:
            return [(b,tail2) for a,tail1 in self.parse(string) for b,tail2 in other.parse(tail1)]
        return Parser(parser)


@Parser
def anySymbol[S](string: List[S]) -> ParseResult[S, S]:
    if string:
        return [(string[0], string[1:])]
    return []


def satisfy[S](f: Callable[[S], bool]) -> Parser[S, S]:
    def parser(string: List[S]) -> ParseResult[S, S]:
        if string and f(string[0]):
            return [(string[0], string[1:])]
        return []

    return Parser(parser)


@Parser
def empty(string: List[Any]) -> ParseResult[Any, Any]:
    return []

@Parser
def eof[S](string:List[Any]) -> ParseResult[Any,tuple[()]]:
    if string == []:
        return [((),[])]
    return []

failp = empty


def succeed[A](a: A) -> Parser[Any, A]:
    def parser[S](string: List[S]) -> ParseResult[S,A]:
        return [(a, string)]

    return Parser(parser)


pure = succeed


def lookahead[S](string: List[S]) -> ParseResult[S, List[S]]:
    return [(list(string), list(string))]


# derived parsers

epsilon: Parser[Any, tuple[()]] = succeed(())


def symbol[S](c: S) -> Parser[S, S]:
    return satisfy((lambda a: a == c))


def token[S](word: List[S]) -> Parser[S, List[S]]:
    accumulator: Parser[S, List[S]] = succeed([])

    for c in word:
        accumulator = symbol(c).liftBinary((lambda x,y: [*y,x]), accumulator)
    return accumulator


def many[S, A](p: Parser[S, A]) -> Parser[S, List[A]]:
    def parser(string: List[S]) -> ParseResult[S, List[A]]:
        tails = [([], string)]
        result = []
        while tails:
            # print(tails)
            vals, t = tails.pop()
            result.append((vals,t))
            tails += [([*vals, v], tail1) for (v, tail1) in p(t)]
        return result

    return Parser(parser)


def some[S, A](p: Parser[S, A]) -> Parser[S, List[A]]:
    return p.liftBinary((lambda x,y:[x,*y]), many(p))
