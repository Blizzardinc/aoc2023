test = False
first_exercise = False
def main():
    filedir = f"inputs/day1{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
    total = 0
    if first_exercise:
        for line in lines:
            first = None
            for c in line:
                if c in "1234567890":
                    last = c
                    if first is None:
                        first = c
            total += int(f"{first}{last}")
    else:
        wordlist = ["one","two","three","four","five","six","seven","eight","nine"]
        digits = [*"123456789"]
        wordtodig = dict(zip(wordlist,digits))
        vals = []
        for x in lines:
            line = x.strip()
            first = None
            hit = None
            while line != "":
                # print((line,first,hit))
                possibword = [ key for key in wordlist if line.startswith(key)]
                if possibword:
                    first = wordtodig[possibword[0]]
                    break
                elif line[0] in digits:
                    first = line[0]
                    break
                else:
                    line = line[1:]
            line = x.strip()
            while line != "":
                possibword = [key for key in wordlist if line.endswith(key)]
                if possibword:
                    last = wordtodig[possibword[0]]
                    break
                elif line[-1] in digits:
                    last = line[-1]
                    break
                else:
                    line = line[:-1]
            total += int(f"{first}{last}")
        # print(vals)
                





    print(total)

if __name__ == "__main__":
    exit(main())

