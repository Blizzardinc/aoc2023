test = False
first_exercise = False

from itertools import cycle

from math import lcm


def main():
    filedir = (
        f"inputs/day8{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    directions, maps = text.split("\n\n")

    maps = {y[:3]: (y[7:10], y[12:15]) for y in maps.split("\n")}
    if first_exercise:
        jumps = 0
        loc = "AAA"
        for d in cycle(directions):
            if loc == "ZZZ":
                print(jumps)
                break
            else:
                m = maps[loc]
                match d:
                    case "L":
                        loc = m[0]
                    case "R":
                        loc = m[1]
                jumps += 1
    else:
        locs = [loc for loc in maps if loc[-1]=="A"]
        loops = [0 for loc in locs]
        for j,d in enumerate(cycle(directions)):
            print(loops)
            if all([l!=0 for l in loops]):
                break
            for i,loc in enumerate(locs):
                if loops[i]==0 and loc[-1]=="Z":
                    loops[i]=j
            match d:
                case "L":
                    locs = [maps[loc][0] for loc in locs]
                
                case "R":
                    locs = [maps[loc][1] for loc in locs]
        a = 1
        for l in loops:
            a = lcm(a,l)
        print(a)

if __name__ == "__main__":
    exit(main())
