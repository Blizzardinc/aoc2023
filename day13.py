test = False
first_exercise = False


def map_has_vert_symmetry(map, f_e):
    possibles = []
    lines = map.split("\n")
    for i, line in enumerate(lines[1:]):
        if len([a for a, b in zip(line, lines[i]) if a != b]) <= (0 if f_e else 1):
            possibles.append(i + 1)  # the number of columns to the left of the mirror
    res = []
    for p in possibles:
        if (
            all(
                [
                    all([a == b for a, b in zip(lines[p - 1 - i], lines[p + i])])
                    for i in range(min(p, len(lines) - p))
                ]
            )
            and f_e
        ):
            return p
        elif not f_e:
            x = [
                    i
                    for i in range(min(p, len(lines) - p))
                    if not all([a == b for a, b in zip(lines[p - 1 - i], lines[p + i])])
                ] # list of i where the lists don't exactly match
            # print(p,x)
            if len(x) == 1 and len([a for a, b in zip(lines[p - 1 - x[0]], lines[p + x[0]]) if a != b]) == 1:
                return p
    return 0


def transmogrify(map):
    lines = map.split("\n")
    # print("\n".join(["".join([line[i] for line in lines]) for i in range(len(lines[0]))]))
    return "\n".join(
        ["".join([line[i] for line in lines]) for i in range(len(lines[0]))]
    )


def main():
    filedir = (
        f"inputs/day13{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read().strip()
        ...

    maps = text.split("\n\n")

    total = 0
    for map in maps:
        r = 100 * map_has_vert_symmetry(map, first_exercise)
        if r:
            total += r
        else:
            total += map_has_vert_symmetry(transmogrify(map), first_exercise)

    print(total)
    # print(map_has_vert_symmetry(maps[0]))
    # print(map_has_vert_symmetry(transmogrify(maps[0])))


if __name__ == "__main__":
    exit(main())
