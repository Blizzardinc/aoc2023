from math import lcm, gcd


test = False
first_exercise = False

def process_first_pulse(modules,queue,destinations,moduletypes,modulestate):
    assert queue
    (origin,modulename,signal) = queue.pop(0)
    if modulename not in modules:
        return modulename,signal
    moduletype = moduletypes[modulename]
    state = modulestate[modulename]
    res = None
    match moduletype:
        case "%":
            if signal:
                res = not state
                modulestate[modulename] = res
        case "&":
            state[origin] = signal
            res = not any(dict.values(state))
        case "broadcaster":
            res = state
    # print(origin,modulename,signal,state,res)
    if res is not None:
        queue += [(modulename,dest,res) for dest in destinations[modulename]]





def main():
    filedir = f"inputs/day20{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...
    
    destinations = {}

    moduletypes = {}

    modulestate = {}
    modules = []
    for line in lines:
        typename,dests = line.strip().split(" -> ")
        if typename == "broadcaster":
            modulename = typename
            moduletype = typename
        else:
            modulename = typename[1:]
            moduletype = typename[0]
        modules.append(modulename)
        destinations[modulename] = dests.split(", ")
        moduletypes[modulename] = moduletype
        if moduletype == "&":
            modulestate[modulename] = {}
        else:
            modulestate[modulename] = True
    for name in modules:
        for dest in destinations[name]:
            if dest in modules and moduletypes[dest] == "&":
                modulestate[dest][name] = True
        

    if first_exercise:
        high = 0
        low = 0
        for _ in range(1000):
            queue = [("button","broadcaster",True)]
            while queue:
                if queue[0][-1]:
                    low += 1
                else:
                    high += 1
                # print(queue)
                process_first_pulse(modules,queue,destinations,moduletypes,modulestate)
        print(high * low, high, low)
    else:
        bd = 0b111110111011 # good, 4027
        rs = 0b111110100011 # good, 4003
        pm = 0b111101001111 # good, 3919
        cc = 0b111101001101 # good, 3917
        numbs= [bd,pm,cc,rs]
        print(numbs, [bin(x) for x in numbs])

        # print([gcd(x,y)==1 for x in numbs for y in numbs])
        i = 0
        for _  in range(bd):
            i += 1
            queue = [("button","broadcaster",True)]
            while queue:
                # if _ == bd-1:
                if queue[0][0] in ["bd", "pm", "cc", "rs"] and queue[0][2]:
                    print(queue[0], i)
                process_first_pulse(modules,queue,destinations,moduletypes,modulestate)
        print(modulestate["bd"])
        print(modulestate["pm"])
        print(modulestate["cc"])
        print(modulestate["rs"])
        print(lcm(bd,pm,cc,rs))


        
if __name__ == "__main__":
    exit(main())
