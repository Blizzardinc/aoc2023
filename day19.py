from typing import Tuple

test = False
first_exercise = False

def range_obj_val(o) -> int:
    res = 1
    for (l,u) in o.values():
        res *= u-l+1
    return res

def apply_rule(rules, object, rulename):
    subrules = rules[rulename]
    for subrule in subrules:
        srres = None
        if ":" in subrule:
            c, potres = subrule.split(":")
            match c[1]:
                case ">":
                    if object[c[0]] > int(c[2:]):
                        srres = potres
                case "<":
                    if object[c[0]] < int(c[2:]):
                        srres = potres
                case x:
                    raise ValueError(f"unknown comparison {x}")
        else:
            srres = subrule
        match srres:
            case None:
                pass
            case "R":
                return "R"
            case "A":
                return "A"
            case x:
                return apply_rule(rules, object, x)
    raise ValueError(f"did not match any rule")

def apply_rule_ranged(rules,sobject,rulename) -> Tuple[int,int]:
    object = {k:v for (k,v) in sobject.items()}

    subrules = rules[rulename]
    atotal = 0
    rtotal = 0
    objects=[object]
    for subrule in subrules:
        remaining_objects = []
        for o in objects:
            print(o)
            # sleep(0.5)
            srres = None
            if ":" in subrule:
                # when needed, split the object, continue with the part that succeeds
                c,pres = subrule.split(":")
                cval = int(c[2:])
                orange = o[c[0]] # haha, o_range = orange
                l,u = orange
                match c[1]:
                    case ">":
                        if l <= cval:
                            # there are numbers which don't satisfy the condition
                            o_copy = {k:v for (k,v) in o.items()}
                            o_copy[c[0]] = (l,min(cval,u))
                            remaining_objects.append(o_copy)
                        if cval < u:
                            o[c[0]] = (max(l,cval+1),u)
                            srres = pres
                    case "<":
                        if u >= cval:
                            # there are numbers which don't satisfy the condition
                            o_copy = {k:v for (k,v) in o.items()}
                            o_copy[c[0]] = (max(l,cval),u)
                            remaining_objects.append(o_copy)
                        if l < cval:
                            # there are numbers which satsify the condition
                            o[c[0]] = (l,min(cval-1,u))
                            srres = pres
            else:
                srres = subrule
            print(o)
            print(srres)
            print(remaining_objects)
            match srres:
                case None:
                    # the entire object fails the condition
                    remaining_objects.append(o)
                case "A":
                    atotal += range_obj_val(o)
                    ...
                case "R":
                    rtotal += range_obj_val(o)
                case x:
                    a,r = apply_rule_ranged(rules,o,x)
                    atotal += a
                    rtotal += r
        objects=remaining_objects
    assert objects == []
    # print(sobject)
    # print(rulename)
    # print(f"{sum([range_obj_val(o) for o in objects])=}")
    # print(f"{atotal + rtotal=}")
    # print(f"{range_obj_val(sobject)=}")
    assert atotal + rtotal + sum([range_obj_val(o) for o in objects]) == range_obj_val(sobject)
    return (atotal,rtotal)


def main():
    filedir = (
        f"inputs/day19{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...

    ruletext, parttext = text.split("\n\n")
    rules = {}
    for ruleword in ruletext.split("\n"):
        name, content = ruleword[:-1].split("{")
        subrules = content.split(",")
        rules[name] = subrules

    parts = []

    for line in parttext.split("\n"):
        # print(line)
        parts.append({n:int(v[2:]) for (n, v) in zip("xmas", line[1:-1].split(","))})

    if first_exercise:
        print(sum([sum(p.values()) for p in parts if apply_rule(rules,p,"in") == "A"]))
    else:
        range_object = {k:(1,4000) for k in "xmas"}
        print(apply_rule_ranged(rules,range_object,"in"))


if __name__ == "__main__":
    exit(main())
