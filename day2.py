test = False
first_exercise = False


class Game:
    id = None

    def __init__(self, id, r, g, b):
        self.id: int = id
        self.r: int = r
        self.g: int = g
        self.b: int = b

    def __repr__(self):
        return f"game(id={self.id},r={self.r},g={self.g},b={self.b})"


def main():
    filedir = (
        f"inputs/day2{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...

    games = []

    for line in lines:
        line = line.strip()[5:]
        (idstr, rest) = line.split(":")
        grabs = rest.split(";")
        assert all([len(grab.split(","))<=3 for grab in grabs])
        words = [word.split(" ") for grab in grabs for word in grab.split(",")]
        # print(words)

        reds = [word[1:] for word in words if word[-1] == "red"]
        # print(reds)
        greens = [word[1:] for word in words if word[-1] == "green"]
        # print(greens)
        blues = [word[1:] for word in words if word[-1] == "blue"]
        # print(blues)

        red = max(int(w[0]) for w in reds)
        green = max(int(w[0]) for w in greens)
        blue = max(int(w[0]) for w in blues)
        game = Game(int(idstr), red, green, blue)
        games.append(game)
    if first_exercise:
        possibles = [
            game for game in games if game.r <= 12 and game.g <= 13 and game.b <= 14
        ]
        print(possibles)
        print(sum(game.id for game in possibles))
    else:
        print(sum(game.r*game.g*game.b for game in games))


if __name__ == "__main__":
    exit(main())
