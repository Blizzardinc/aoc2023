test = False
first_exercise = False

def hash(val):
    current_val =0

    for c in val:
        current_val += ord(c)
        current_val *= 17
        current_val %= 256
    
    return current_val

class hashmap:
    boxes = [[] for _ in range(256)]

    def dash(self, label):
        box = self.boxes[hash(label)] 
        lensmatches = [(i,lens) for i,lens in enumerate(box) if lens[1] == label]
        if lensmatches:
            val = lensmatches[0]
            self.boxes[hash(label)] = [*box[:val[0]],*box[val[0]+1:]]
            return val
        return None
    
    def equals(self,label,focalstrenth):
        box = self.boxes[hash(label)]
        lensmatches = [(i,lens) for i,lens in enumerate(box) if lens[1] == label]
        if lensmatches:
            box[lensmatches[0][0]] = (focalstrenth,label)
        else:
            box.append((focalstrenth,label))

    def focus_value(self):
        total = 0
        for i,box in enumerate(self.boxes):
            for j,lens in enumerate(box):
                total += (i + 1) * (j + 1) * lens[0]
        return total
    def __string__(self):
        return str(self.boxes)


def main():
    filedir = f"inputs/day15{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    
    text = "".join(text.split("\n")) # yeet all newlines
    steps = text.split(",")
        
    if first_exercise:
        print(sum([hash(c) for c in steps]))
    else:
        m = hashmap()

        for c in steps:
            if "-" in c:
                m.dash(c.split("-")[0])
            elif "=" in c:
                label,value = c.split("=")
                m.equals(label,int(value))
        print(*[f"{i}, {box}" for i,box in enumerate(m.boxes) if box != []], sep = "\n")
        
        print(m.focus_value())

if __name__ == "__main__":
    exit(main())
