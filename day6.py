test = False
first_exercise = False

import math as m

def main():
    filedir = f"inputs/day6{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        times,distances = f.readlines()
        # text = f.read()
        ...


    if first_exercise:
        # solve: x*(7-x) > d
        # for integers
        # same as: find integers x between
        # 
        # for (t,d) in zip(times,distances):
        #     x1 = ((t-sqrt(t**2-4*d)))
        #     x2 = ((t+sqrt(t**2-4*d)))
        times = [int(x) for x in times.split(":")[1].split(" ") if x!=""]
        distances = [int(x) for x in distances.split(":")[1].split(" ") if x!=""]
        ...
    else:
        times = [int("".join((x for x in times.split(":")[1] if x != " ")))]
        distances = [int("".join((x for x in distances.split(":")[1] if x != " ")))]

        ...

    l = ((((t-m.sqrt(t**2-4*d))/2),((t+m.sqrt(t**2-4*d))/2)) for (t,d) in zip(times,distances))
    # print(*iter(l))
    p=1
    for (x1,x2) in l:
        x = m.ceil(x2-1) - m.floor(x1+1) + 1
        if x<=0:
            p=0
            break
        else:
            p*=x
    print(p)

        
if __name__ == "__main__":
    exit(main())
