test = False
first_exercise = False

def transmogrify(map):
    lines = map.split("\n")
    # print(map,end="\n\n")
    # print("test")
    # print("\n".join(["".join([line[i] for line in lines]) for i in range(len(lines[0]))]), end="\n\n")
    return "\n".join(
        ["".join([line[i] for line in lines]) for i in range(len(lines[0]))]
    )

def flip_vertical(map):
    val = "\n".join(list(map.split("\n"))[::-1])
    return val

def rotate_right(map):
    return transmogrify(flip_vertical(map))

def rotate_left(map):
    assert flip_vertical
    return flip_vertical(transmogrify(map))

def swipe_left(map):
    newlines = []
    for line in map.split("\n"):
        linelen = len(line)
        newline = []
        last_edge = 0
        for i,c in enumerate(line):
            match c:
                case "#":
                    newline += [*["." for _ in range(i-len(newline))], "#"]
                    assert len(newline) == i + 1
                    last_edge = i+1
                case "O":
                    last_edge += 1
                    newline.append("O")
                case ".":
                    pass
        newline += ["." for _ in range(linelen - len(newline))]
        newlines.append("".join(newline))
    map = "\n".join(newlines)
    # print(map)
    return map

def calc_west_weight(map):
    total = 0
    stuff = map
    for line in stuff.split("\n"):
        linelen = len(line)
        for i,c in enumerate(line):
            match c:
                case "O":
                    total += linelen-i
                case _: 
                    pass
    return total

def main():
    filedir = f"inputs/day14{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read().strip()
        ...

    if first_exercise:
        map = transmogrify(text)
        stuff = swipe_left(map)
        print(calc_west_weight(stuff))
    else:
        print(text, end="\n\n")
        print(flip_vertical(text), end="\n\n")
        # print(transmogrify(text), end="\n\n")
        # print(rotate_left(text), end="\n\n")
        # print(rotate_right(text), end="\n\n")
        stuff = rotate_left(text)
        # print(stuff, end="\n\n")
        boards = {}
        x = 0
        while stuff not in boards and x != 1_000_000_000:
            if x % 100000 == 0:
                # print(stuff)
                ...
            boards[stuff] = x
            x += 1
            for _ in range(4):
                map = swipe_left(stuff)
                # print(map)
                stuff = rotate_right(map)
            # print(stuff, end="\n\n")
        if x == 1_000_000_000:
            ...
            # print("fucking hell")
            # print(rotate_right(stuff), end="\n\n")
        else:
            # print("quick out1")
            looplen = x - boards[stuff]
            # print(looplen)
            steps_todo = (1000_000_000 - x) % looplen
            # print(steps_todo)
            # print(stuff)
            for _ in range(steps_todo * 4):
                map = swipe_left(stuff)
                # print(map)
                stuff = rotate_right(map)
                # print(stuff)
            # print("quick out2")
            print(calc_west_weight(stuff))
            # stuff = rotate_right(stuff)
            # print(stuff, end="\n\n")


if __name__ == "__main__":
    exit(main())
