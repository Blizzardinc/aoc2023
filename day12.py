test = False
first_exercise = False

from parsing.core import symbol, many, some, succeed, eof
from functools import lru_cache


def main():
    filedir = (
        f"inputs/day12{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...

    total = 0
    working = symbol(".") | symbol("?")
    broken = symbol("#") | symbol("?")
    for line in lines:
        # print(line)
        stuff, describe = line.strip().split(" ")
        params = [int(c) for c in describe.split(",")]
        if first_exercise:
            print(stuff)
            print(params)
            parser_accumulator = many(working).ignoreThen(succeed([]))
            for par in params[:-1]:
                for _ in range(par):
                    parser_accumulator = parser_accumulator.liftBinary(
                        (lambda x, y: [*x, "#"]), broken
                    )
                parser_accumulator = parser_accumulator.liftBinary(
                    (lambda x, y: [*x, *["." for c in y]]), some(working)
                )
            for _ in range(params[-1]):
                parser_accumulator = parser_accumulator.liftBinary(
                    (lambda x, y: [*x, "#"]), broken
                )
            parser_accumulator = parser_accumulator.liftBinary(
                (lambda x, y: [*x, *["." for c in y]]), many(working)
            )
            parser_accumulator = parser_accumulator.thenIgnore(eof)

            result = parser_accumulator.parse(list(stuff))
            # # print(*result, sep="\n")
            # print(*[("".join(gs), r) for (gs, r) in result], sep="\n")
            print(len(result), end="\n\n")
            total += len(result)
        else:
            stuff = "?".join([stuff for _ in range(5)])
            stuff = ".".join([s for s in stuff.split(".") if s != ""])
            params = params * 5
            print(stuff)
            print(params)
            @lru_cache
            def calc_stuff(row, hint) -> int:
                if hint == len(params):
                    if "#" in stuff[row:]:
                        return 0
                    else:
                        return 1
                else:
                    if len(stuff) - row < params[hint]:
                        return 0
                    else:
                        res = 0
                        if stuff[row] != "#":
                            res += calc_stuff(row+1,hint)
                        if not "." in stuff[row: row+params[hint]]:
                            if len(stuff) > row + params[hint] and stuff[row+params[hint]] != "#":
                                res += calc_stuff(row+params[hint]+1,hint+1)
                            elif len(stuff) <= row + params[hint]:
                                res += calc_stuff(row+params[hint],hint+1)
                        # print(stuff[row:],params[hint:],res)
                        return res
            print(calc_stuff(0,0))
            # print(calc_stuff(2,0))
            # print(calc_stuff)
            total += calc_stuff(0,0)
    print(total)


if __name__ == "__main__":
    exit(main())
