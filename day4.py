test = False
first_exercise = False

def main():
    filedir = f"inputs/day4{('a' if first_exercise and test else 'b') if test else ''}.txt"
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        lines = f.readlines()
        # text = f.read()
        ...
    
    cards = []

    for line in lines:
        id, tail1 = line.strip().split(":")
        id = int(id[5:])

        winnings, yours = tail1.split("|")
        wins = [int(w) for w in winnings.strip().split(" ") if w != ""]
        yous = {int(y) for y in yours.strip().split(" ") if y != ""}
        cards.append((wins,yous))

    numwins = [len([y for y in yous if y in wins]) for (wins,yous) in cards]

    if first_exercise:
        print(sum([2**(x-1) for x in numwins if x != 0]))
    else:
        numbers = [1 for n in numwins]

        for i, n in enumerate(numwins):
            for j in range(n):
                if i+j+1 < len(numwins):
                    numbers[i+j+1] += numbers[i]
                else:
                    break
        print(sum(numbers))

        
if __name__ == "__main__":
    exit(main())
