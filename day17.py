from time import sleep


test = False
first_exercise = False


def main():
    filedir = (
        f"inputs/day17{('a' if first_exercise and test else 'b') if test else ''}.txt"
    )
    if test:
        filedir = "test" + filedir

    with open(filedir) as f:
        # lines = f.readlines()
        text = f.read()
        ...
    m = text.split("\n")
    y_max = len(m)
    x_max = len(m[0])

    dirs = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    if first_exercise:
        startpos = (0, 0)
        direction = (0, 0)
        visited = set()
        startingpath = (velocity, direction, startpos)
        velocity = 0
        boundary = [startingpath]
        distances = {(v, d, startpos): 0 for d in dirs for v in range(1, 4)}
        distances[startingpath] = 0
        while True:
            c_path = boundary.pop(0)
            c_velocity, c_dir, c_pathend = c_path
            (dy, dx) = c_dir
            (y, x) = c_pathend
            if c_pathend == (y_max - 1, x_max - 1):
                print(f"distance is {distances[c_path]}")
                break
            for i in range(c_velocity, 4):
                visited.add((i, c_dir, c_pathend))

            if c_velocity == 3:
                possible_dirs = [d for d in dirs if d not in [(dy, dx), (-dy, -dx)]]
            else:
                possible_dirs = [d for d in dirs if d != (-dy, -dx)]

            filtered_dirs = [
                (dy, dx)
                for (dy, dx) in possible_dirs
                if 0 <= y + dy < y_max
                if 0 <= x + dx < x_max
            ]
            for dy, dx in filtered_dirs:
                new_position = (y + dy, x + dx)
                extra_distance = int(m[y + dy][x + dx])
                new_velocity = 1 if (dy, dx) != c_dir else c_velocity + 1
                new_path = (new_velocity, (dy, dx), new_position)
                if new_path not in visited:
                    if new_path not in boundary:
                        boundary.append(new_path)
                    if new_path in distances:
                        if distances[new_path] > distances[c_path] + extra_distance:
                            with open("out.txt", "a") as f:
                                f.write(
                                    f"{distances[c_path]};{c_path}->{new_path};{distances[new_path]}\n"
                                )
                            # print(f"rather go to {new_position} from {c_pathend}")
                        distances[new_path] = min(
                            distances[new_path], distances[c_path] + extra_distance
                        )

                    else:
                        distances[new_path] = distances[c_path] + extra_distance
                        with open("out.txt", "a") as f:
                            f.write(
                                f"{distances[c_path]};{c_path}->{new_path};{distances[new_path]}\n"
                            )

            boundary.sort(key=lambda x: distances[x])
        print("final path calculation:")
    else:
        boundary = [((0, 0), False), ((0, 0), True)]

        distances = {v: 0 for v in boundary}
        visited = set()

        while True:
            # print(boundary)
            # if len(boundary) > 10:
            #     exit()
            point = boundary.pop(0)
            ((y, x), horizontal) = point
            if (y, x) == (y_max - 1, x_max - 1):
                print(distances[point])
                break
            visited.add(point)

            if horizontal:
                neighbours = [((y, x + i), False) for i in range(4, 11)] + [
                    ((y, x - i), False) for i in range(4, 11)
                ]
            else:
                neighbours = [((y + i, x), True) for i in range(4, 11)] + [
                    ((y - i, x), True) for i in range(4, 11)
                ]
            # print(neighbours)
            filtered_neighbours = sorted(
                [
                    o
                    for ((ny, nx), b) in neighbours
                    if (0 <= ny < y_max and 0 <= nx < x_max)
                    and (o := ((ny, nx), b)) not in visited
                ],
                key=lambda x: int(m[x[0][0]][x[0][1]]),
            )
            for neighbour in filtered_neighbours:
                (ny, nx), b = neighbour
                if horizontal:
                    assert abs(x-nx) in [-10,-9,-8,-7,-6,-5,-4,4,5,6,7,8,9,10]
                    # print(f"{abs(x-nx)=}")
                    extradist = sum(
                        [int(m[ny][i]) for i in range(nx, x, (x - nx) // abs(x - nx))]
                    )
                else:
                    assert abs(y-ny) in [-10,-9,-8,-7,-6,-5,-4,4,5,6,7,8,9,10]
                    # print(f"{abs(y-ny)=}")
                    extradist = sum(
                        [int(m[i][nx]) for i in range(ny, y, (y - ny) // abs(y - ny))]
                    )
                newdistance = distances[point] + extradist
                if neighbour in visited:
                    continue
                if neighbour not in distances:
                    distances[neighbour] = newdistance
                    boundary.append(neighbour)
                    continue
                olddistance = distances[neighbour]
                if olddistance > newdistance:
                    distances[neighbour] = newdistance
                    # if the distance already is known, it already is in the boundary
                    assert neighbour in boundary
            boundary.sort(key=lambda x:distances[x])

if __name__ == "__main__":
    exit(main())
